#include <QCoreApplication>
#include <QNetworkInterface>
#include <QHostAddress>
#include <QList>
#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QList<QHostAddress> addrs = QNetworkInterface::allAddresses();
    foreach(QHostAddress s, addrs){
        if(s.protocol() == QAbstractSocket::IPv4Protocol &&
                s != QHostAddress(QHostAddress::LocalHost))

            qDebug()<< s.toString();
    }

    return a.exec();
}
